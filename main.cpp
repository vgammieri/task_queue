#include <iostream>
#include <boost/program_options.hpp>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/bind.hpp>

#include "task_queue.h"

using std::cerr;
using std::cout;
using std::endl;

namespace po = boost::program_options;
namespace io= boost::asio;


#include "ToDo.h"

#define EXPECT_EQUAL(test, expect) equalityTest( test,  expect, \
#test, #expect, \
__FILE__, __LINE__)

template < typename T1, typename T2 >
int equalityTest(const T1    testValue,
                 const T2    expectedValue,
                 const char* testName,
                 const char* expectedName,
                 const char* fileName,
                 const int   lineNumber);

boost::mutex io_mutex;

void init_options(po::variables_map & vm, int argc, char ** argv){
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "produce help message")
    ("threads", po::value<int>()->default_value(3), "set working threads")
    ;
    
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help")){
        cout << desc;
        exit (1);
    }
    
}

void run_in_thread(const std::string& printme){
    boost::this_thread::sleep_for(boost::chrono::seconds(1));
    io_mutex.lock();
    cout << "T: " << boost::this_thread::get_id()<< " - " << printme << endl;
    io_mutex.unlock();
}

int main(
         int    argc,
         char** argv
         )
{
    po::variables_map vm;
    
    init_options(vm, argc, argv);
    
    TaskQueue queue;
    
    
    int t=vm["threads"].as<int>();
    queue.allocate(t);
    
    for (int i=0; i<t; ++i) {
        queue.post_task(boost::bind(run_in_thread,(std::string("Thread ")+std::to_string(i))));
    }
    
    queue.post_task(boost::bind(run_in_thread, "Additional task 1"));
    queue.post_task(boost::bind(run_in_thread, "Additional task 2"));
    queue.post_task(boost::bind(run_in_thread, "Additional task 3"));
    queue.wait_all_tasks();
    
    
    int result = 0;
    
    ToDo list;
    
    list.addTask("write code");
    list.addTask("compile");
    list.addTask("test");
    
    result |= EXPECT_EQUAL(list.size(),     3);
    result |= EXPECT_EQUAL(list.getTask(0), "write code");
    result |= EXPECT_EQUAL(list.getTask(1), "compile");
    result |= EXPECT_EQUAL(list.getTask(2), "test");
    
    if (result == 0)
    {
        cout << "Test passed" << endl;
    }
    
    return result;
}


template < typename T1, typename T2 >
int equalityTest(
                 const T1    testValue,
                 const T2    expectedValue,
                 const char* testName,
                 const char* expectedName,
                 const char* fileName,
                 const int   lineNumber
                 )
{
    if (testValue != expectedValue)
    {
        cerr << fileName << ":" << lineNumber << ": "
        << "Expected " << testName << " "
        << "to equal " << expectedName << " (" << expectedValue << ") "
        << "but it was (" << testValue << ")" << endl;
        
        return 1;
    }
    else
    {
        return 0;
    }
}
