//
//  queue.h
//  ToDoList
//
//  Created by Vincenzo Gammieri on 26/07/14.
//
//

#ifndef __ToDoList__queue__
#define __ToDoList__queue__

#include <boost/noncopyable.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/thread.hpp>
#include <boost/scoped_ptr.hpp>

class TaskQueue : boost::noncopyable{
public:
    TaskQueue():
    _ios(),
    _work(new boost::asio::io_service::work(_ios)),
    _num_of_threads(0){};
    
    void allocate(int num_of_threads){
        _num_of_threads = num_of_threads;
        for(int i = 0; i<_num_of_threads;++i){
            _thread_group.create_thread(boost::bind(&boost::asio::io_service::run, &_ios));
        }
    }
    
    
    template <typename CompletionHandler>
    void post_task(CompletionHandler task){
        _ios.post(task);
    }
    
    void wait_all_tasks(bool join_all_threads = true){
        if (!_work){
            return;
        }
        release();
        _ios.run();
        _ios.stop();
        _thread_group.join_all();
    }
    
    ~TaskQueue(){
        wait_all_tasks(false);
    }
private:
    
    void release(){
        _work.reset();
        
    };
    
private:
    boost::asio::io_service _ios;
    boost::scoped_ptr<boost::asio::io_service::work> _work;
    boost::thread_group _thread_group;
    int _num_of_threads;
    
};


#endif /* defined(__ToDoList__queue__) */
