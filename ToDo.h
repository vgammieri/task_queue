#ifndef TODO_H
#define TODO_H

#include <string>
#include <boost/container/vector.hpp>
#include <boost/noncopyable.hpp>

namespace bc = boost::container;

class ToDo : public boost::noncopyable
{
public:
    ToDo();
    ~ToDo();

    size_t size() const;

    void addTask(const std::string& task);
    
    std::string getTask(size_t index) const;

private:

    bc::vector<std::string> this_tasks;
};

#endif // TODO_H
